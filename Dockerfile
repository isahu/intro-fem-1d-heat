FROM python:slim

ADD src/fem1d_bvp_linear.py /

RUN pip install numpy scipy matplotlib

CMD [ "python3", "fem1d_bvp_linear.py" ]
