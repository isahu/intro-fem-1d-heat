---
author:
- Indrajeet Sahu
date: June 2020
title: FEM 1D Boundary Value Problem
---

**FEM1D\_BVP\_LINEAR**

**Finite Element Method, 1D, Boundary Value Problem, Piecewise Linear
Elements**

The [boundary value problem
](https://people.sc.fsu.edu/~jburkardt/py_src/fem1d_bvp_linear/fem1d_bvp_linear.html)(BVP)
-

$\displaystyle -\frac{d}{dx}\left( a( x)\frac{du}{dx}\right) +c( x) u( x) =f( x)$,
$\displaystyle 0< x< 1$

$\displaystyle a( x) ,\ c( x) ,\ f( x) \ :\ $given functions

Boundary Conditions: $\displaystyle u( 0) =u( 1) =0$

[**Solution**:]{.underline}

Discretize the domain into N nodes and assume approximating solution to
be of the form

$$u( x) \ =\ \sum ^{N}_{j=1} u_{j} v_{j}( x)$$where,
$\displaystyle v_{j}( x)$ represent piecewise linear basis functions and
$\displaystyle u_{j}$ are unknown coefficients to be determined.

Our piecewise linear basis functions are: $$v_{i}( x) =\begin{cases}
\frac{x_{i+1} -x}{x_{i+1} -x_{i}} & ,\ x_{i} \leqslant x< x_{i+1}\\
\frac{x -x_{i-1}}{x_{i} -x_{i-1}} & ,\ x_{i-1} \leqslant x< x_{i}
\end{cases}$$ Proceed with multiplying the BVP by a trial/test function
$\displaystyle v_{j}( x)$ such that it satisfies the Dirichlet boundary
conditions in the BVP.
$$\int ^{1}_{0} v_{j}( x)\left\{-\frac{d}{dx}\left( a( x)\frac{du}{dx}\right) +c( x) u( x) -f( x)\right\} dx\ =\ 0$$
Using integration by parts
$$\left[ v_{j}( x) a( x)\frac{du}{dx}\right]^{1}_{0} +\int ^{1}_{0} v_{j} '( x) a( x)\frac{du}{dx} dx\ +\ \int ^{1}_{0} c( x) u( x) v_{j}( x) dx\ =\ \int ^{1}_{0} v_{j}( x) f( x) dx$$
Applying the Dirichlet Boundary conditions on test function,

$$\begin{gathered}
{\displaystyle \ } \notag\\
{\displaystyle \int ^{1}_{0} v_{j} '( x) a( x)\frac{du}{dx} dx\ +\ \int ^{1}_{0} c( x) u( x) v_{j}( x) dx\ =\ \int ^{1}_{0} v_{j}( x) f( x) dx} \notag\\
{\displaystyle \Longrightarrow \int ^{1}_{0} v_{j} '( x) a( x)\frac{d( u_{1} v_{1} +u_{2} v_{2} +...+u_{N} v_{N})}{dx} dx+\int ^{1}_{0} c( x)\{u_{1} v_{1} +u_{2} v_{2} +...+u_{N} v_{N}\} v_{j}( x) dx\ } \notag\\
{\displaystyle =\ \int ^{1}_{0} v_{j}( x) f( x) dx\ \ \ \ \ \ \ \ \ ,j=1,2,...,N}\\
 \notag\end{gathered}$$This gives us N equations for each j, thus we can
solve for N unknown coefficients $\displaystyle u_{j}$.

Next task, is to construct matrix representation for these N equations.

We note that corresponding to each trial function $\displaystyle v_{j}$,
the non-vanishing terms inside the two integrals in LHS would be those
with $\displaystyle v_{j-1}$,
$\displaystyle v_{j}$,$\displaystyle v_{j+1}$.

So, eqns.(5) become $$\begin{gathered}
{\displaystyle \int ^{1}_{0} v_{j} '( x) a( x)\frac{d( u_{j-1} v_{j-1} +u_{j} v_{j} +u_{j+1} v_{j+1})}{dx} dx+\int ^{1}_{0} c( x)\{u_{j-1} v_{j-1} +u_{j} v_{j} +u_{j+1} v_{j+1}\} v_{j}( x) dx\ } \notag\\
{\displaystyle =\ \int ^{1}_{0} v_{j}( x) f( x) dx\ \ \ \ \ \ \ \ \ ,j=1,2,...,N}\end{gathered}$$

![Schematic of the Linear Basis Functions](image.png){#fig:my_label
width="3in"}

As these are N nodes in the discretized domain, so we can break the
limits of integration corresponding to each finite element. Writing the
system in matrix form, we have

$$KU=F$$where, K =

$$\begin{bmatrix}
\int ^{2nd\ node}_{1st\ node} a( x) v_{1} 'v_{1} '\ +cv_{!} v_{1} & \int ^{2nd\ node}_{1st\ node} av_{1} 'v_{1} '+cv_{1} v_{2} & 0 & 0 & . & 0 & 0\\
\int ^{3rd\ node}_{1st\ node} av_{1} 'v_{2} '+cv_{1} v_{2} & \int ^{3rd\ node}_{1st\ node} av_{2} 'v_{2} '+cv_{2} v_{2} & \int ^{3rd\ node}_{1st\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & 0 & . & 0 & 0\\
0 & \int ^{4th\ node}_{2nd\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & \int ^{4th\ node}_{2nd\ node} av_{3} 'v_{3} '+cv_{3} v_{3} & \int ^{4th\ node}_{2nd\ node} av_{3} 'v_{4} '+cv_{3} v_{4} & . & 0 & 0\\
0 & 0 & . & . & . & 0 & 0\\
. & . & . & . & . & . & .\\
0 & 0 & 0 & 0 & 0 & \int ^{Nth\ node}_{N-2\ node} av^{'}_{N-1} v^{'}_{N} +cv^{'}_{N-1} v^{'}_{N} & \int ^{Nth\ node}_{N-2\ node} av^{'}_{N} v^{'.}_{N} +cv_{N} v_{N}
\end{bmatrix}$$

$$\begin{gathered}
U=\begin{Bmatrix}
u_{1}\\
u_{2}\\
.\\
.\\
.\\
u_{N}
\end{Bmatrix} ,\ F=\begin{Bmatrix}
{\textstyle \int ^{2nd\ node}_{1st\ node}} fv_{1}\\
{\textstyle \int ^{3rd\ node}_{1st\ node} fv_{2}}\\
0\\
0\\
0\\
{\textstyle \int ^{Nth\ node}_{N-2\ node} fv_{N}}
\end{Bmatrix}\end{gathered}$$\
Applying the Dirichlet Boundary Conditions, the matrix K becomes
$$\begin{gathered}
\begin{bmatrix}
0 & 0 & 0 & 0 & . & 0 & 0\\
\int ^{3rd\ node}_{1st\ node} av_{1} 'v_{2} '+cv_{1} v_{2} & \int ^{3rd\ node}_{1st\ node} av_{2} 'v_{2} '+cv_{2} v_{2} & \int ^{3rd\ node}_{1st\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & 0 & . & 0 & 0\\
0 & \int ^{4th\ node}_{2nd\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & \int ^{4th\ node}_{2nd\ node} av_{3} 'v_{3} '+cv_{3} v_{3} & \int ^{4th\ node}_{2nd\ node} av_{3} 'v_{4} '+cv_{3} v_{4} & . & 0 & 0\\
0 & 0 & . & . & . & 0 & 0\\
. & . & . & . & . & . & .\\
0 & 0 & 0 & 0 & 0 & 0 & 0
\end{bmatrix}\end{gathered}$$

This K matrix can be seen to be composed of integration from
$\displaystyle 1^{st}$node to the last $\displaystyle N^{th}$ node.\
\
We can write it as a summation of different matrices for each finite
element, i.e. from $\displaystyle j^{th}$node to
$\displaystyle ( j+1)^{th} \ $node, i.e.
$\displaystyle {\textstyle K=\sum ^{N}_{i=1} K_{i}}$

So,

-   For first finite elmenet, i.e. from node 1 to node 2

$$\begin{gathered}
K_{1} =\begin{bmatrix}
\int ^{2nd\ node}_{1st\ node} a( x) v_{1} 'v_{1} '\ +cv_{!} v_{1} & \int ^{2nd\ node}_{1st\ node} av_{1} 'v_{1} '+cv_{1} v_{2} & 0 & 0 & . & 0 & 0\\
\int ^{2nd\ node}_{1st\ node} av_{1} 'v_{2} '+cv_{1} v_{2} & \int ^{2nd\ node}_{1st\ node} av_{2} 'v_{2} '+cv_{2} v_{2} & 0 & 0 & . & 0 & 0\\
0 & 0 & 0 & 0 & . & 0 & 0\\
0 & 0 & . & . & . & 0 & 0\\
. & . & . & . & . & . & .\\
0 & 0 & 0 & 0 & 0 & 0 & 0
\end{bmatrix} \ ,\ F_{1} =\begin{Bmatrix}
\int ^{2nd\ node}_{1st\ node} fv_{1}\\
0\\
0\\
.\\
.\\
0
\end{Bmatrix}\\
\\\end{gathered}$$

-   For the second finite element, i.e. from node 2 to node 3,

$$\begin{gathered}
K_{2} =\begin{bmatrix}
0 & 0 & 0 & 0 & . & 0 & 0\\
0 & \int ^{3rd\ node}_{2nd\ node} av_{2} 'v_{2} '+cv_{2} v_{2} & \int ^{3rd\ node}_{2nd\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & 0 & . & 0 & 0\\
0 & \int ^{3rd\ node}_{2nd\ node} av_{2} 'v_{3} '+cv_{2} v_{3} & \int ^{3rd\ node}_{2nd\ node} av_{3} 'v_{3} '+cv_{3} v_{3} & 0 & . & 0 & 0\\
0 & 0 & . & . & . & 0 & 0\\
. & . & . & . & . & . & .\\
0 & 0 & 0 & 0 & 0 & 0 & 0
\end{bmatrix} \ ,\ F_{2} \ =\ \begin{Bmatrix}
0\\
\int ^{3rd\ node}_{2nd\ node} fv_{2}\\
\int ^{3rd\ node}_{2nd\ node} fv_{3}\\
.\\
.\\
0
\end{Bmatrix}\\
\\\end{gathered}$$

-   and so on\...

\
Each of the integral inside matrices can be evaluated using two-point
Gauss quadrature formula, i.e.,

$$\int ^{b}_{a} f( x) dx=\ w_{1} f( x_{1}) +w_{2} f( x_{2}) =\left(\frac{b-a}{2}\right) f\left(\frac{b-a}{2}\left( -\frac{1}{\sqrt{3}}\right) +\frac{b+a}{2}\right) +\left(\frac{b-a}{2}\right) f\left(\frac{b-a}{2}\left(\frac{1}{\sqrt{3}}\right) +\frac{b+a}{2}\right)$$\
\
Finally, we end up with the system of linear equations represented by
$\displaystyle KU=F$, which can be solved to determine the unknown
coefficients $\displaystyle u_{1}$, $\displaystyle u_{2}$, \... ,
$\displaystyle u_{N}$.

If $\displaystyle a( x) =c( x) =1$ and $\displaystyle f( x) =x$, then,
the Boundary Value Problem becomes $$-\frac{d^{2} u}{dx^{2}} +u=x$$ Its
exact solution is:  $\displaystyle u=x-\frac{sinh( x)}{sinh( 1)}$\
\
**Computation of Errors arising out of this approximate solution:**

-   $\displaystyle L_{1}$ norm =
    $\displaystyle \sum ^{N}_{i=1}| u_{i} -u_{exact,\ i}|$

-   $\displaystyle L_{2}$ norm =
    $\displaystyle \int ^{1}_{0}( u-u_{exact})^{2} dx$ ,\[Note: This
    integral is evaluated using two-point Gauss quadrature\]

-   H1 seminorm of error: H1S =
    $\displaystyle \int ^{1}_{0}\left( u'\ -\ u^{'}_{exact}\right)^{2} dx$

-   Maximum norm of error =
    $\displaystyle \int ^{1}_{0}\max(| u-u_{exact}| )\ dx$
