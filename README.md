[![pipeline status](https://gitlab.com/scientific-computing1/intro-fem-1d-heat/badges/master/pipeline.svg)](https://gitlab.com/scientific-computing1/intro-fem-1d-heat/-/commits/master)

# 1D heat equation solved by FEM

This repository uses the python script from [John Burkardt](https://people.sc.fsu.edu/~jburkardt/py_src/fem1d_bvp_linear/fem1d_bvp_linear.py) to solve a simple heat problem.
Gitlab is used to run the tests and publish the resulting images.

This math is inline $`- \frac{\partial^2 u}{\partial x^2} + u = x`$.

This is on a separate line

```math
x - \frac{\sinh(x)}{\sinh(1)}
```

The solution is:

![1DSolution](https://scientific-computing1.gitlab.io/intro-fem-1d-heat/fem1d_bvp_linear_test00.png)

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright

Copyright 2020 Mattia Montanari
